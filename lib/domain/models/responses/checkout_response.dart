import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';

part 'checkout_response.g.dart';

@JsonSerializable()
class CheckoutResponse extends Equatable {
  final int statusCode;
  final CheckoutDetail checkout;

  const CheckoutResponse({
    required this.statusCode,
    required this.checkout,
  });

  factory CheckoutResponse.fromMap(Map<String, dynamic> json) =>
      _$CheckoutResponseFromJson(json);

  Map<String, dynamic> toMap() => _$CheckoutResponseToJson(this);

  @override
  List<Object> get props => [statusCode, checkout];
}

@JsonSerializable()
class CheckoutDetail extends Equatable {
  @JsonKey(name: 'total_price')
  final double totalPrice;
  final List<CheckoutProduct> items;

  const CheckoutDetail({
    required this.totalPrice,
    required this.items,
  });

  factory CheckoutDetail.fromJson(Map<String, dynamic> json) =>
      _$CheckoutDetailFromJson(json);

  Map<String, dynamic> toJson() => _$CheckoutDetailToJson(this);

  @override
  List<Object> get props => [totalPrice, items];
}

@JsonSerializable()
class CheckoutProduct extends Equatable {
  final String name;
  final int quantity;
  final double price;
  final String description;
  final String image;
  final int id;
  @JsonKey(name: 'total_price')
  final double totalPrice;

  const CheckoutProduct({
    required this.name,
    required this.quantity,
    required this.price,
    required this.description,
    required this.image,
    required this.id,
    required this.totalPrice,
  });

  factory CheckoutProduct.fromJson(Map<String, dynamic> json) =>
      _$CheckoutProductFromJson(json);

  Map<String, dynamic> toJson() => _$CheckoutProductToJson(this);

  @override
  List<Object> get props =>
      [name, quantity, price, description, image, id, totalPrice];
}
