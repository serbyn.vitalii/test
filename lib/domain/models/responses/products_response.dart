import 'package:json_annotation/json_annotation.dart';

part 'products_response.g.dart';

@JsonSerializable()
class ProductsResponse {
  final int statusCode;
  final List<Product> products;

  ProductsResponse({
    required this.statusCode,
    required this.products,
  });

  factory ProductsResponse.fromMap(Map<String, dynamic> json) =>
      _$ProductsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ProductsResponseToJson(this);
}

@JsonSerializable()
class Product {
  final String name;
  int quantity;
  final String description;
  final String image;
  final int id;

  Product({
    required this.name,
    required this.quantity,
    required this.description,
    required this.image,
    required this.id,
  });

  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);

  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
