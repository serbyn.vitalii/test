// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'checkout_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckoutResponse _$CheckoutResponseFromJson(Map<String, dynamic> json) =>
    CheckoutResponse(
      statusCode: json['statusCode'] as int,
      checkout:
          CheckoutDetail.fromJson(json['checkout'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CheckoutResponseToJson(CheckoutResponse instance) =>
    <String, dynamic>{
      'statusCode': instance.statusCode,
      'checkout': instance.checkout,
    };

CheckoutDetail _$CheckoutDetailFromJson(Map<String, dynamic> json) =>
    CheckoutDetail(
      totalPrice: (json['total_price'] as num).toDouble(),
      items: (json['items'] as List<dynamic>)
          .map((e) => CheckoutProduct.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CheckoutDetailToJson(CheckoutDetail instance) =>
    <String, dynamic>{
      'total_price': instance.totalPrice,
      'items': instance.items,
    };

CheckoutProduct _$CheckoutProductFromJson(Map<String, dynamic> json) =>
    CheckoutProduct(
      name: json['name'] as String,
      quantity: json['quantity'] as int,
      price: (json['price'] as num).toDouble(),
      description: json['description'] as String,
      image: json['image'] as String,
      id: json['id'] as int,
      totalPrice: (json['total_price'] as num).toDouble(),
    );

Map<String, dynamic> _$CheckoutProductToJson(CheckoutProduct instance) =>
    <String, dynamic>{
      'name': instance.name,
      'quantity': instance.quantity,
      'price': instance.price,
      'description': instance.description,
      'image': instance.image,
      'id': instance.id,
      'total_price': instance.totalPrice,
    };
