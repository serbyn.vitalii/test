import 'package:json_annotation/json_annotation.dart';

part 'checkout_request.g.dart';

@JsonSerializable()
class CheckoutRequest {
  final List<CheckoutItem> items;

  CheckoutRequest({required this.items});

  factory CheckoutRequest.fromJson(Map<String, dynamic> json) =>
      _$CheckoutRequestFromJson(json);

  //Map<String, dynamic> toJson() => _$CheckoutRequestToJson(this);

  Map<String, dynamic> toMap() => _$CheckoutRequestToJson(this);
}

@JsonSerializable(explicitToJson: true)
class CheckoutItem {
  final int quantity;
  @JsonKey(name: 'product_id')
  final int productId;

  CheckoutItem({required this.quantity, required this.productId});

  factory CheckoutItem.fromJson(Map<String, dynamic> json) =>
      _$CheckoutItemFromJson(json);

  Map<String, dynamic> toMap() => _$CheckoutItemToJson(this);
}
