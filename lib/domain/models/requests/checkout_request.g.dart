// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'checkout_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckoutRequest _$CheckoutRequestFromJson(Map<String, dynamic> json) =>
    CheckoutRequest(
      items: (json['items'] as List<dynamic>)
          .map((e) => CheckoutItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CheckoutRequestToJson(CheckoutRequest instance) =>
    <String, dynamic>{
      'items': instance.items,
    };

CheckoutItem _$CheckoutItemFromJson(Map<String, dynamic> json) => CheckoutItem(
      quantity: json['quantity'] as int,
      productId: json['product_id'] as int,
    );

Map<String, dynamic> _$CheckoutItemToJson(CheckoutItem instance) =>
    <String, dynamic>{
      'quantity': instance.quantity,
      'product_id': instance.productId,
    };
