import 'package:salonkee_test/domain/models/responses/products_response.dart';

import '../../utils/resources/data_state.dart';
import '../models/requests/checkout_request.dart';
import '../models/responses/checkout_response.dart';

abstract class ApiRepository {
  Future<DataState<ProductsResponse>> getProducts();

  Future<DataState<CheckoutResponse>> checkout({
    required CheckoutRequest request,
  });
}
