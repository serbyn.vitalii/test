import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:salonkee_test/domain/models/requests/checkout_request.dart';

import '../../presentation/views/products_screen.dart';
import '../../presentation/views/checkout_screen.dart';

part 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  RouteType get defaultRouteType => const RouteType.material();

  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: ProductsRoute.page, initial: true),
        AutoRoute(page: CheckoutRoute.page),
      ];
}

final appRouter = AppRouter();
