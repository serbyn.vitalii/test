import 'package:salonkee_test/domain/models/requests/checkout_request.dart';
import 'package:salonkee_test/domain/models/responses/products_response.dart';

import '../../domain/models/responses/checkout_response.dart';
import '../../domain/repositories/api_repository.dart';
import '../../utils/resources/data_state.dart';
import '../datasources/remote/salonkee_api_service.dart';
import 'base/base_api_repository.dart';

class ApiRepositoryImpl extends BaseApiRepository implements ApiRepository {
  final SalonkeeApiService _salonkeeApiService;

  ApiRepositoryImpl(this._salonkeeApiService);

  @override
  Future<DataState<CheckoutResponse>> checkout({
    required CheckoutRequest request,
  }) {
    return getStateOf<CheckoutResponse>(
      request: () => _salonkeeApiService.checkout(request.items),
    );
  }

  @override
  Future<DataState<ProductsResponse>> getProducts() {
    return getStateOf<ProductsResponse>(
      request: () => _salonkeeApiService.getRemoteProducts(),
    );
  }
}
