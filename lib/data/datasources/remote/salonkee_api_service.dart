import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:salonkee_test/domain/models/requests/checkout_request.dart';
import 'package:salonkee_test/domain/models/responses/products_response.dart';

import '../../../domain/models/responses/checkout_response.dart';
import '../../../utils/constants/strings.dart';

part 'salonkee_api_service.g.dart';

@RestApi(baseUrl: baseUrl, parser: Parser.MapSerializable)
abstract class SalonkeeApiService {
  factory SalonkeeApiService(Dio dio, {String baseUrl}) = _SalonkeeApiService;

  @GET('/products')
  Future<HttpResponse<ProductsResponse>> getRemoteProducts();

  @POST('/checkout')
  Future<HttpResponse<CheckoutResponse>> checkout(
      @Body() List<CheckoutItem> items);
}
