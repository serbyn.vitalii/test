import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:oktoast/oktoast.dart';
import 'package:salonkee_test/config/router/app_router.dart';
import 'package:salonkee_test/config/themes/app_theme.dart';
import 'package:salonkee_test/domain/repositories/api_repository.dart';
import 'package:salonkee_test/locator.dart';
import 'package:salonkee_test/presentation/cubits/checkout/checkout_cubit.dart';
import 'package:salonkee_test/presentation/cubits/products/products_cubit.dart';
import 'package:salonkee_test/utils/constants/strings.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initializeDependencies();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => CheckoutCubit(
            locator<ApiRepository>(),
          ),
        ),
        BlocProvider(
          create: (context) => ProductsCubit(
            locator<ApiRepository>(),
          )..getProducts(),
        )
      ],
      child: OKToast(
        child: MaterialApp.router(
          debugShowCheckedModeBanner: false,
          routerDelegate: appRouter.delegate(),
          routeInformationParser: appRouter.defaultRouteParser(),
          title: appTitle,
          theme: AppTheme.light,
        ),
      ),
    );
  }
}
