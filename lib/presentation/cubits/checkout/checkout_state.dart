part of 'checkout_cubit.dart';

abstract class CheckoutState extends Equatable {
  final CheckoutDetail details;
  final DioException? error;

  const CheckoutState(
      {this.details = const CheckoutDetail(totalPrice: 0, items: []),
      this.error});

  @override
  List<Object> get props => [details];
}

class CheckoutLoading extends CheckoutState {
  const CheckoutLoading();
}

class CheckoutSuccess extends CheckoutState {
  const CheckoutSuccess({super.details});
}

class CheckoutFailed extends CheckoutState {
  const CheckoutFailed({super.error});
}
