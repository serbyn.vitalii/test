import 'dart:async';

import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:salonkee_test/domain/models/requests/checkout_request.dart';
import 'package:salonkee_test/domain/models/responses/checkout_response.dart';
import 'package:salonkee_test/domain/repositories/api_repository.dart';
import 'package:salonkee_test/utils/resources/data_state.dart';

part 'checkout_state.dart';

class CheckoutCubit extends Cubit<CheckoutState> {
  final ApiRepository _apiRepository;

  CheckoutCubit(this._apiRepository) : super(const CheckoutLoading());

  Future<void> checkout({required List<CheckoutItem> checkoutItems}) async {
    final response = await _apiRepository.checkout(
        request: CheckoutRequest(
      items: checkoutItems,
    ));

    if (response is DataSuccess) {
      final checkout = response.data!.checkout;

      emit(CheckoutSuccess(details: checkout));
    } else if (response is DataFailed) {
      emit(CheckoutFailed(error: response.error));
    }
  }
}
