import 'package:salonkee_test/presentation/cubits/base/ui_effect.dart';

class NotEnoughProductsFailed implements UiEffect {
  String failureText;
  NotEnoughProductsFailed(this.failureText);
}
