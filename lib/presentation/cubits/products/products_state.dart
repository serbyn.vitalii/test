part of 'products_cubit.dart';

abstract class ProductsState extends Equatable {
  final List<Product> products;
  final List<CheckoutItem> checkoutItems;
  final DioException? error;

  const ProductsState({
    this.products = const [],
    this.checkoutItems = const [],
    this.error,
  });

  @override
  List<Object?> get props => [products, checkoutItems, error];
}

class ProductsLoading extends ProductsState {
  const ProductsLoading();
}

class ProductsSuccess extends ProductsState {
  const ProductsSuccess({super.products, super.checkoutItems});

  ProductsState copyWith({
    List<CheckoutItem>? checkoutItems,
    List<Product>? products,
  }) {
    return ProductsSuccess(
      products: products ?? this.products,
      checkoutItems: checkoutItems ?? this.checkoutItems,
    );
  }
}

class ProductsFailed extends ProductsState {
  const ProductsFailed({super.error});
}
