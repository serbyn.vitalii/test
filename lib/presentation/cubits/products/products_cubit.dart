import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:salonkee_test/domain/models/requests/checkout_request.dart';
import 'package:salonkee_test/domain/models/responses/products_response.dart';
import 'package:salonkee_test/presentation/cubits/base/cubit_with_effects.dart';
import 'package:salonkee_test/presentation/cubits/base/ui_effect.dart';
import 'package:salonkee_test/presentation/cubits/products/products_effects.dart';

import '../../../domain/repositories/api_repository.dart';
import '../../../utils/resources/data_state.dart';

part 'products_state.dart';

class ProductsCubit extends CubitWithEffects<ProductsState, UiEffect> {
  final ApiRepository _apiRepository;

  ProductsCubit(this._apiRepository) : super(const ProductsLoading());

  Future<void> getProducts() async {
    final response = await _apiRepository.getProducts();

    if (response is DataSuccess) {
      final products = response.data!.products;

      emit(ProductsSuccess(products: products));
    } else if (response is DataFailed) {
      emit(ProductsFailed(error: response.error));
    }
  }

  void addProductToCart(Product product) {
    if (notEnoughQuantity(product)) {
      emitEffect(NotEnoughProductsFailed('Not enough products'));
      return;
    }

    final currentState = state;
    if (currentState is ProductsSuccess) {
      final currentItems = List<CheckoutItem>.from(currentState.checkoutItems);
      final index =
          currentItems.indexWhere((element) => element.productId == product.id);

      if (index != -1) {
        final existingItem = currentItems[index];
        final updatedItem = CheckoutItem(
          quantity: existingItem.quantity + 1,
          productId: existingItem.productId,
        );
        currentItems[index] = updatedItem;
      } else {
        currentItems.add(CheckoutItem(quantity: 1, productId: product.id));
      }
      product.quantity--;

      emit(currentState.copyWith(
        checkoutItems: currentItems,
        products: state.products,
      ));
    }
  }

  void clearCart() {
    final currentState = state;
    if (currentState is ProductsSuccess) {
      emit(currentState.copyWith(checkoutItems: []));
    }
  }

  bool notEnoughQuantity(Product product) {
    return product.quantity < 1;
  }
}
