import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ionicons/ionicons.dart';
import 'package:salonkee_test/domain/models/requests/checkout_request.dart';
import 'package:salonkee_test/domain/models/responses/products_response.dart';
import 'package:salonkee_test/presentation/cubits/products/products_cubit.dart';
import 'package:salonkee_test/presentation/cubits/products/products_effects.dart';

import '../../config/router/app_router.dart';
import '../widgets/product_widget.dart';

@RoutePage()
class ProductsScreen extends StatefulWidget {
  const ProductsScreen({Key? key}) : super(key: key);

  @override
  State<ProductsScreen> createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  late ProductsCubit cubit;

  @override
  void initState() {
    super.initState();
    cubit = BlocProvider.of<ProductsCubit>(context);
    cubit.effectStream.listen((effect) {
      if (effect is NotEnoughProductsFailed) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Not enough products in stock.'),
            backgroundColor: Colors.red,
          ),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProductsCubit, ProductsState>(
      builder: (_, state) {
        switch (state.runtimeType) {
          case ProductsLoading:
            return const Center(child: CupertinoActivityIndicator());
          case ProductsFailed:
            return const Center(child: Icon(Ionicons.refresh));
          case ProductsSuccess:
            final checkoutQuantity =
                state.checkoutItems.fold(0, (int sum, CheckoutItem item) {
              return sum + item.quantity;
            });
            return Scaffold(
              appBar: AppBar(
                title: const Text(
                  'Salonkee Products',
                  style: TextStyle(color: Colors.black),
                ),
                actions: [
                  GestureDetector(
                    onTap: () => cubit.clearCart(),
                    child: const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 14),
                      child: Icon(Ionicons.trash_outline, color: Colors.black),
                    ),
                  ),
                ],
              ),
              body: _buildProducts(state.products),
              bottomNavigationBar: AnimatedContainer(
                duration: const Duration(milliseconds: 300),
                height: state.checkoutItems.isNotEmpty ? 80 : 0,
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 16, bottom: 32, left: 24, right: 24),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text('Items in basket: $checkoutQuantity'),
                      ),
                      GestureDetector(
                        onTap: () => appRouter.push(
                            CheckoutRoute(checkoutItems: state.checkoutItems)),
                        child: const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 14),
                          child: Icon(Ionicons.arrow_forward_circle,
                              color: Colors.black),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );

          default:
            return const SizedBox();
        }
      },
    );
  }

  Widget _buildProducts(List<Product> products) {
    return CustomScrollView(
      slivers: [
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, index) =>
                ProductItem(product: products[index], onRemove: (e) {}),
            childCount: products.length,
          ),
        ),
      ],
    );
  }
}
