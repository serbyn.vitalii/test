import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ionicons/ionicons.dart';
import 'package:salonkee_test/domain/models/responses/checkout_response.dart';
import 'package:salonkee_test/presentation/cubits/checkout/checkout_cubit.dart';
import 'package:salonkee_test/domain/models/requests/checkout_request.dart';
import 'package:salonkee_test/presentation/widgets/checkout_item_widget.dart';

import '../../config/router/app_router.dart';

@RoutePage()
class CheckoutScreen extends StatefulWidget {
  final List<CheckoutItem> checkoutItems;
  const CheckoutScreen({
    Key? key,
    required this.checkoutItems,
  }) : super(key: key);

  @override
  State<CheckoutScreen> createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  @override
  void initState() {
    super.initState();
    final cubit = context.read<CheckoutCubit>();
    cubit.checkout(checkoutItems: widget.checkoutItems);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () => appRouter.pop(),
          child: const Icon(Ionicons.chevron_back, color: Colors.black),
        ),
        title: const Text(
          'Purchase products',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: BlocBuilder<CheckoutCubit, CheckoutState>(
        builder: (_, state) {
          switch (state.runtimeType) {
            case CheckoutLoading:
              return const Center(child: CupertinoActivityIndicator());
            case CheckoutSuccess:
              return _buildPurchaseList(state.details.items);
            default:
              return const SizedBox();
          }
        },
      ),
    );
  }

  Widget _buildPurchaseList(List<CheckoutProduct> products) {
    if (products.isEmpty) {
      return const Center(
          child: Text(
        'No products for checkout',
        style: TextStyle(color: Colors.black),
      ));
    }

    return ListView.builder(
      itemCount: products.length,
      itemBuilder: (context, index) {
        return CheckoutItemWidget(product: products[index]);
      },
    );
  }
}
