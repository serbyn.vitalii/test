import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ionicons/ionicons.dart';
import 'package:salonkee_test/domain/models/responses/products_response.dart';
import 'package:salonkee_test/presentation/cubits/products/products_cubit.dart';

class ProductItem extends StatelessWidget {
  final Product product;
  final bool isRemovable;
  final void Function(Product product)? onRemove;

  const ProductItem({
    Key? key,
    required this.product,
    this.isRemovable = false,
    this.onRemove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _onTap,
      child: Container(
        padding: const EdgeInsetsDirectional.only(
            start: 14, end: 14, bottom: 7, top: 7),
        height: 140,
        child: Row(
          children: [
            _buildImage(context),
            _buildTitleAndDescription(),
            _buildAddToCart(context),
          ],
        ),
      ),
    );
  }

  Widget _buildImage(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.only(end: 14),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20.0),
        child: Container(
          width: MediaQuery.of(context).size.width / 3,
          height: double.maxFinite,
          decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.08),
          ),
          child: Image.network(
            product.image,
            fit: BoxFit.cover,
            errorBuilder: (_, __, ___) {
              return const Center(
                child: Text(
                  '404\nNot found',
                  textAlign: TextAlign.center,
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _buildTitleAndDescription() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 7),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              product.name,
              maxLines: 3,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                fontFamily: 'Butler',
                fontWeight: FontWeight.w900,
                fontSize: 18,
                color: Colors.black87,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Text(
                  product.description,
                  maxLines: 2,
                ),
              ),
            ),
            Row(
              children: [
                const Icon(Ionicons.grid_outline, size: 16),
                const SizedBox(width: 4),
                Text(
                  product.quantity.toString(),
                  style: const TextStyle(
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAddToCart(BuildContext context) {
    final cubit = context.read<ProductsCubit>();
    return GestureDetector(
      onTap: () => cubit.addProductToCart(product),
      child: const Padding(
        padding: EdgeInsets.symmetric(horizontal: 14),
        child: Icon(Ionicons.cart, color: Colors.black),
      ),
    );
  }

  void _onTap() {
    // TODO on tap
  }

  void _onRemove() {
    if (onRemove != null) {
      onRemove?.call(product);
    }
  }
}
