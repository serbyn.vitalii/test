import 'package:awesome_dio_interceptor/awesome_dio_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:salonkee_test/utils/constants/strings.dart';

import 'data/datasources/remote/salonkee_api_service.dart';
import 'data/repositories/api_repository_impl.dart';
import 'domain/repositories/api_repository.dart';

final locator = GetIt.instance;

Future<void> initializeDependencies() async {
  final dio = Dio();
  dio.interceptors.add(AwesomeDioInterceptor());
  dio.options.headers['content-Type'] = 'application/json';
  dio.options.headers["x-api-key"] = xApiKey;

  locator.registerSingleton<Dio>(dio);

  locator.registerSingleton<SalonkeeApiService>(
    SalonkeeApiService(locator<Dio>()),
  );

  locator.registerSingleton<ApiRepository>(
    ApiRepositoryImpl(locator<SalonkeeApiService>()),
  );
}
